<?php

namespace KDA\Rsync;


use Closure;
use KDA\Laravel\Shell\Facades\ShellCommand;
use KDA\Rsync\Traits\WithHostKey;
use Illuminate\Support\Traits\Conditionable;
class Rsync
{
    use WithHostKey;
    use Conditionable;
    
    protected ?string $ssh_key_path = null;
    protected ?string $hardlink_source_path = null;
    protected RsyncEndpoint $source;
    protected RsyncEndpoint $destination;
    protected ?array $excludes = null;
    protected ?string $link_dest = null;

    public static function make():static 
    {
        return app()->make(static::class);
    }

    public function getSource(): RsyncEndpoint
    {
        return $this->source;
    }

    public function getDestination(): RsyncEndpoint
    {
        return $this->destination;
    }
    public function getSSHKeyPath():?string{
        return $this->ssh_key_path;
    }

    public function hasSSHKey(): bool
    {
        return !blank($this->getSSHKeyPath());
    }

    public function getExcludes(): ?array
    {
        return $this->excludes;
    }

    public function getHardlinkSourcePath(): ?string
    {
        return $this->hardlink_source_path;
    }

    public function source(RsyncEndpoint $source): static
    {
        $this->source = $source;
        return $this;
    }

    public function destination(RsyncEndpoint $destination): static
    {
        $this->destination = $destination;
        return $this;
    }

    public function sshKeyPath(string $key_path): static
    {
        $this->ssh_key_path = $key_path;
        return $this;
    }

    public function excludes(?array $excludes): static
    {
        $this->excludes = $excludes;
        return $this;
    }

    public function hardlinkSourcePath(?string $hardlink_source): static
    {
        $this->hardlink_source_path = $hardlink_source;
        return $this;
    }

    public function hasHardlink():bool{
        return !blank($this->hardlink_source_path);
    }

    public function getCommand(): string
    {
        $__command = 'rsync -r -t -v -z -s --progress %s %s %s %s %s';
        $host_key = $this->getCurrentHostKey();
        $source_part = (string)$this->getSource()->when(!blank($host_key),fn($o)=>$o->withHostKey($host_key));
        $dest_part = (string)$this->getDestination()->when(!blank($host_key),fn($o)=>$o->withHostKey($host_key));
        $ssk_key_part = $this->hasSSHKey() ? '-e ' . escapeshellarg('ssh -i ' . $this->getSSHKeyPath()) : '';
        $hardlink_part = $this->hasHardlink() ? '--link-dest=' . escapeshellarg($this->getHardlinkSourcePath())  :'';

        $excludes = $this->getExcludes() ? collect($this->getExcludes())->map(function ($item) {
            return '--exclude=' . escapeshellarg($item);
        })->join(' ') : '';
        $cmd = sprintf(
            $__command,
            $ssk_key_part,
            $hardlink_part,
            $excludes,
            $source_part,
            $dest_part,
        );
        return $cmd;
    }

    public function execute(): static
    {
        $command = $this->getCommand();
        $output = ShellCommand::execute($command);
        return $this;
    }
}
