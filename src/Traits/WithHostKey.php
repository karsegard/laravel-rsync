<?php 
namespace KDA\Rsync\Traits;


trait WithHostKey{


    public ?string $current_host_key = null;

    public function getCurrentHostKey():?string
    {
        return $this->current_host_key;
    }

    public function withHostKey(?string $key):static{
        $this->current_host_key = $key;
        return $this;
    }
}