<?php 
namespace KDA\Rsync\Traits;


trait HostKey{

    public ?string $host_key = null;

    public function getHostKey():?string
    {
        return $this->host_key;
    }

    public function hostKey(?string $key):static
    {
        $this->host_key = $key;
        return $this;
    }

}