<?php
namespace KDA\Rsync\Traits;
use Illuminate\Support\Facades\Storage;
trait Chroot{

    protected ?string $chroot=null;


    public function chroot(?string $chroot):static
    {
        $this->chroot = $chroot;
        return $this;
    }

    public function getChroot():?string
    {
        return $this->chroot;
    }
    function joinPathes() {
        $paths = array();
    
        foreach (func_get_args() as $arg) {
            if ($arg !== '') { $paths[] = $arg; }
        }
    
        return preg_replace('#/+#','/',join('/', $paths));
    }
}