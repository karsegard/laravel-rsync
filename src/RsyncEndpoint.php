<?php

namespace KDA\Rsync;


use Closure;
use Illuminate\Support\Facades\Storage;
use InvalidArgumentException;
use KDA\Laravel\Shell\Facades\ShellCommand;
use KDA\Rsync\Traits\HostKey;
use KDA\Rsync\Traits\Chroot;
use KDA\Rsync\Traits\WithHostKey;
use Illuminate\Support\Traits\Conditionable;

class RsyncEndpoint
{
    use HostKey;
    use WithHostKey;
    use Chroot;
    use Conditionable;
    
    public string $path;
    public ?string $user=null;
    public ?string $host=null;
    public ?string $disk = null;
    public bool $escape_path = false;

    public function escape_path(bool $bool):static{
        $this->escape_path = $bool;
        return $this;
    }
    public function path(string $path):static
    {
        $this->path = $path;
        return $this;
    }

    public function host(?string $host):static
    {
        $this->host = $host;
        return $this;
    }

    public function user(?string $user):static
    {
        $this->user = $user;
        return $this;
    }

    public function disk (string $disk):static
    {
        $this->disk = $disk;
        return $this;
    }

    public function __construct(?string $path=null,?string $chroot=null)
    {
        $this->chroot($chroot);
        if(!blank($path)){
        $this->path($path);
    }
    }

    public static function make(?string $path=null,?string $chroot=null):static{
        return app()->make(static::class,['path'=>$path,'chroot'=>$chroot]);
    }

    public function getDisk():?string{
        return $this->disk;
    }
    public function hasDisk():bool{
        return !blank($this->getDisk());
    }

    public function getUser():?string{
        return $this->user;
    }

    public function getHost():?string{
        return $this->host;
    }
    

    public function getPath():string {
        return $this->hasDisk() ? Storage::disk($this->getDisk())->path($this->path) : $this->path;
    }

    public function __toString()
    {
        $path = $this->getPath();

        $user = $this->getUser();
        $host = $this->getHost();

        $chroot = $this->getChroot();

        if(!blank($chroot)){
            $path = $this->joinPathes($chroot,$path);
        }

        if(!blank($user) && blank($host)){
            throw new InvalidArgumentException('user must be used with host');
        }

        $host_key = $this->getHostKey();
        $current_host_key = $this->getCurrentHostKey();

    //    dump($host_key, !blank($host_key) , !blank($current_host_key) , blank($host), $host_key == $current_host_key);
        if(!blank($host_key) && !blank($current_host_key) && !blank($host) && $host_key == $current_host_key){
            $host = null;
        }else if(!blank($host_key) && !blank($current_host_key) && blank($host) && $host_key != $current_host_key){
            throw new InvalidArgumentException('host key has been applied without any host configured, that means you are potentially trying to sync with a local only point');

        }

     



        $path = $this->escape_path ? escapeshellarg($path) : $path;

        if(!blank($host) && !blank($user)){
            return "{$user}@{$host}:".$path;
        }else if(!blank($host)){
            return "{$host}:".$path;
        }

        return $path;
        
    }
}