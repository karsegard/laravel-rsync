<?php

namespace KDA\Rsync\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;

class PullCommand extends Command
{
     /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:rsync:pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'pull';


    public function __construct(Filesystem $files)
    {
        parent::__construct();

    }


    public function fire()
    {
        return $this->handle();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        
    }
}
