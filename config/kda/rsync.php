<?php

return [
    'remotes' => [
        'preprod' => [
            'host' => 'foil.ftp.infomaniak.com',
            'cwd' => '/home/clients/0bb2b1a3a75b831ccc437a339d8ffc2a/sites/2022.theatreorangerie.ch'
        ]

    ],
    'rsync' => [
        'images'=>[
            'source'=>'database/seeds/*_preprod2022_*',
            'dest'=>'database/seeds'
        ]
    ]
];
