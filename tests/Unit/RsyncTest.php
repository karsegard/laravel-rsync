<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\BackupServer\Models\Backup;
use KDA\Laravel\BackupServer\Models\BackupDestination;
use KDA\Laravel\BackupServer\Models\BackupDestinationStat;
use KDA\Laravel\BackupServer\Models\BackupLog;
use KDA\Laravel\BackupServer\Models\BackupServer;
use KDA\Laravel\BackupServer\Models\BackupSource;
use KDA\Rsync\Rsync;
use KDA\Rsync\RsyncEndpoint;
use KDA\Tests\TestCase;
use InvalidArgumentException;

class RsyncTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_command()
    {
        $rsync = Rsync::make()->source(RsyncEndpoint::make('storage/app/dumps/prod')->host('freya.karsegard.dev'))->destination(RsyncEndpoint::make('storage/app/dumps'))->hardlinkSourcePath('backups');
        $this->assertEquals("rsync -r -t -v -z -s --progress  --link-dest='backups'  freya.karsegard.dev:storage/app/dumps/prod storage/app/dumps", $rsync->getCommand());
    }

    /** @test */
    public function can_create_command_host_key()
    {
        $source = RsyncEndpoint::make('storage/app/dumps/prod')->host('freya.karsegard.dev')->hostKey('freya');
        $destination = RsyncEndpoint::make('storage/app/dumps')->host('freya.karsegard.dev')->hostKey('freya');;

        $rsync = Rsync::make()->withHostKey('freya')->source($source)->destination($destination)->hardlinkSourcePath('backups');
        $this->assertEquals("rsync -r -t -v -z -s --progress  --link-dest='backups'  storage/app/dumps/prod storage/app/dumps", $rsync->getCommand());
    }

    /** @test */
    public function can_create_command_chroot()
    {
        $source = RsyncEndpoint::make('storage/app/dumps/prod')->chroot('/var/www/preprod');
        $destination = RsyncEndpoint::make('storage/app/dumps')->chroot('/var/www/prod');

        $rsync = Rsync::make()->source($source)->destination($destination)->hardlinkSourcePath('backups');
        $this->assertEquals("rsync -r -t -v -z -s --progress  --link-dest='backups'  /var/www/preprod/storage/app/dumps/prod /var/www/prod/storage/app/dumps", $rsync->getCommand());
    }

    /** @test */
    public function test_case_3()
    {
        /**
         * 3 env on 2 hosts
         * prod on  srv1
         * preprod on srv1
         * local on local
         * 
         */
        $prod = RsyncEndpoint::make(chroot: '/var/www/prod')->host('server.example.com')->hostKey('server')->user('hello_world');
        $preprod = RsyncEndpoint::make(chroot: '/var/www/preprod')->host('server.example.com')->hostKey('server')->user('hello_world');
        $local = RsyncEndpoint::make(chroot: '/home/blabla/work')->hostKey('local');

        // sync prod to local
        $rsync = Rsync::make()->source($prod->path('storage/app/dumps/prod'))->destination($local->path('storage/app/dumps'));
        $this->assertEquals("rsync -r -t -v -z -s --progress    hello_world@server.example.com:/var/www/prod/storage/app/dumps/prod /home/blabla/work/storage/app/dumps", $rsync->getCommand());

        // sync preprod to prod from local
        $rsync = Rsync::make()->source($preprod->path('storage/app/dumps/prod'))->destination($prod->path('storage/app/dumps'));
        $this->assertEquals("rsync -r -t -v -z -s --progress    hello_world@server.example.com:/var/www/preprod/storage/app/dumps/prod hello_world@server.example.com:/var/www/prod/storage/app/dumps", $rsync->getCommand());

        // sync preprod to prod from server
        $rsync = Rsync::make()->withHostKey('server')->source($preprod->path('storage/app/dumps/prod'))->destination($prod->path('storage/app/dumps'));
        $this->assertEquals("rsync -r -t -v -z -s --progress    /var/www/preprod/storage/app/dumps/prod /var/www/prod/storage/app/dumps", $rsync->getCommand());



        // sync prod to local from server (impossible because local is not reachable through ssh)
        $this->expectException(InvalidArgumentException::class);
        $rsync = Rsync::make()->withHostKey('server')->source($preprod->path('storage/app/dumps/prod'))->destination($local->path('storage/app/dumps'));
        $rsync->getCommand();
        //$this->assertEquals("rsync -r -t -v -z -s --progress    /var/www/preprod/storage/app/dumps/prod /var/www/prod/storage/app/dumps",$rsync->getCommand());

    }
}
